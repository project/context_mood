CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainers:

* [yannickoo](http://drupal.org/user/531118)

This module provides a context reaction which allows you to display an entity
in a block. This can be used for mood images like header images do this.

INSTALLATION
------------

1. Install the module the drupal way [2]

2. Create a new Context and select Mood as reaction

3. Select the entity type, enter the entity id and select the view mode

[1] http://drupal.org/documentation/install/modules-themes/modules-7
